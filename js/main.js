if(document.readyState === 'complete' || (document.readyState !== 'loading' &&
!document.documentElement.doScroll)){
  initApp();
} else {
  document.addEventListener('DOMContentLoaded', initApp);
}


function initApp(){

  $('.invitation-code--item.input-code i').on('click', function(){
    if($(this).prev().is(':disabled')){
      $(this).prev().attr('disabled', false);
    }else{
      $(this).prev().attr('disabled', true);
    }

  });

  $('.search-items__carousel').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
  });

  var picker = $('input[name="daterange"]').daterangepicker({
    "locale" : {
    "format" : "DD/MM/YYY",
    "separator" : " - ",
    "applyLabel" : "Aplicar",
    "cancelLabel" : "Cancelar",
    "fromLabel" : "From",
    "toLabel" : "To",
    "customRangeLabel" : "Custom",
    "weekLabel" : "W",
    "daysOfWeek" : [ "L", "M", "M", "J", "V",
    "S", "D" ],
    "monthNames" : [ "Enero", "Febero", "Marzo", "Abril",
    "Mayo", "Junio", "Julio", "Agosto", "Septienbre",
    "Octubre", "Noviembre", "Diciembre" ],
    "firstDay" : 1
    },
    "autoApply": true,
    "parentEl": ".calendarMultiple",
    "startDate": moment(),
}, function(start, end, label) {

  console.log('New date range selected: ' + start.format('DD-MM-YYYY') + ' to ' + end.format('DD-MM-YYYY') + ' (predefined range: ' + label + ')');
});

// range update listener
picker.on('apply.daterangepicker', function(ev, picker) {
  $('input[name="daterange"]').val('' + picker.startDate.format('DD/MM/YYY') + ' - ' + picker.endDate.format('DD/MM/YYY'));
});

// prevent hide after range selection
  $('input[name="daterange"]').data('daterangepicker').hide = function () {};
// show picker on load
  $('input[name="daterange"]').data('daterangepicker').show();

$('input[name="daterangeDates"]').daterangepicker({
  "alwaysShowCalendars": true,
  "locale" : {
  "format" : "DD/MM/YYY",
  "separator" : " - ",
  "applyLabel" : "Aplicar",
  "cancelLabel" : "Cancelar",
  "fromLabel" : "From",
  "toLabel" : "To",
  "customRangeLabel" : "Custom",
  "weekLabel" : "W",
  "daysOfWeek" : [ "L", "M", "M", "J", "V",
  "S", "D" ],
  "monthNames" : [ "Enero", "Febero", "Marzo", "Abril",
  "Mayo", "Junio", "Julio", "Agosto", "Septienbre",
  "Octubre", "Noviembre", "Diciembre" ],
  "firstDay" : 1
  },
  "parentEl": ".calendarMultiple",
  "startDate": moment(),
}, function(start, end, label) {
console.log('New date range selected: ' + start.format('DD-MM-YYYY') + ' to ' + end.format('DD-MM-YYYY') + ' (predefined range: ' + label + ')');
});


$('input[name="daterangehour"], input[name="daterangeDeadline"]').daterangepicker({
  "timePicker": true,
  "locale" : {
  "format" : "DD/MM hh:mm A",
  "separator" : " - ",
  "applyLabel" : "Aplicar",
  "cancelLabel" : "Cancelar",
  "fromLabel" : "From",
  "toLabel" : "To",
  "customRangeLabel" : "Custom",
  "weekLabel" : "W",
  "daysOfWeek" : [ "L", "M", "M", "J", "V",
  "S", "D" ],
  "monthNames" : [ "Enero", "Febero", "Marzo", "Abril",
  "Mayo", "Junio", "Julio", "Agosto", "Septienbre",
  "Octubre", "Noviembre", "Diciembre" ],
  "firstDay" : 1
  },
  "startDate": moment().startOf('hour'),
  "endDate": moment().startOf('hour').add(32, 'hour')
}, function(start, end, label) {
console.log('New date range selected: ' + start.format('DD-MM-YYYY') + ' to ' + end.format('DD-MM-YYYY') + ' (predefined range: ' + label + ')');
});

var availableTags = [
  "Polanco",
  "Coyoacan",
];
$( "#zona" ).autocomplete({
  source: availableTags
});


  // Bar Scrolling

    // new SimpleBar(document.getElementById('myTeams__list'), { autoHide: false });


  // new SimpleBar(document.getElementById('countrySearch'), { autoHide: false });
  // new SimpleBar(document.querySelectorAll('.scrollingBar'), { autoHide: false });



  // Slider range Edades
  $( "#slider-range" ).slider({
    range: true,
    min: 15,
    max: 40,
    values: [ 15, 40 ],
    slide: function( event, ui ) {
      $( "#age-desde" ).val( ui.values[ 0 ] );
      $( "#age-hasta" ).val( ui.values[ 1 ] );

    }

  });

  $( "#age-desde" ).val( $( "#slider-range" ).slider( "option", "min" ) );
  $( "#age-hasta" ).val( $( "#slider-range" ).slider( "option", "max" ) );

  // Slider range capacidad
  $( "#slider-capacidad" ).slider({
    range: true,
    min: 1000,
    max: 40000,
    values: [ 2000, 30000 ],
    slide: function( event, ui ) {
      $( "#capacidad-desde" ).val( ui.values[ 0 ] );
      $( "#capacidad-hasta" ).val( ui.values[ 1 ] );
    }

  });

  $( "#capacidad-desde" ).val( $( "#slider-capacidad" ).slider( "option", "min" ) );
  $( "#capacidad-hasta" ).val( $( "#slider-capacidad" ).slider( "option", "max" ) );

  // Slider range capacidad promedio
  $( "#slider-promedio" ).slider({
    range: true,
    min: 1000,
    max: 40000,
    values: [ 2000, 30000 ],
    slide: function( event, ui ) {
      $( "#promedio-desde" ).val( ui.values[ 0 ] );
      $( "#promedio-hasta" ).val( ui.values[ 1 ] );
    }

  });

  $( "#promedio-desde" ).val( $( "#slider-promedio" ).slider( "option", "min" ) );
  $( "#promedio-hasta" ).val( $( "#slider-promedio" ).slider( "option", "max" ) );

  // Slider range costo
  $( "#slider-costo" ).slider({
    range: true,
    min: 1000,
    max: 40000,
    values: [ 2000, 30000 ],
    slide: function( event, ui ) {
      $( "#costo-desde" ).val( ui.values[ 0 ] );
      $( "#costo-hasta" ).val( ui.values[ 1 ] );
    }

  });

  $( "#costo-desde" ).val( $( "#slider-costo" ).slider( "option", "min" ) );
  $( "#costo-hasta" ).val( $( "#slider-costo" ).slider( "option", "max" ) );

  // Slider range presupuesto
  $( "#slider-presupuesto" ).slider({
    range: true,
    min: 0,
    max: 10000,
    values: [ 0, 10000 ],
    slide: function( event, ui ) {
      $( "#presupuesto-desde" ).val( ui.values[ 0 ] );
      $( "#presupuesto-hasta" ).val( ui.values[ 1 ] );
    }

  });

  $( "#presupuesto-desde" ).val( $( "#slider-presupuesto" ).slider( "option", "min" ) );
  $( "#presupuesto-hasta" ).val( $( "#slider-presupuesto" ).slider( "option", "max" ) );

}

function initMap(){

  var mapOptions = {
    center: new google.maps.LatLng(19.3583388, -99.272993),
    zoom: 5,
    minZoom: 5,
    mapTypeControl: false
  };



  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

  // Create the search box and link it to the UI element.
  var input = /** @type {HTMLInputElement} */(
      document.getElementById('search-input'));
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  var gmarkers1 = [];
  var styles = [
      {
          "featureType": "all",
          "elementType": "labels.text.fill",
          "stylers": [
              {
                  "saturation": 36
              },
              {
                  "color": "#333333"
              },
              {
                  "lightness": 40
              }
          ]
      },
      {
          "featureType": "all",
          "elementType": "labels.text.stroke",
          "stylers": [
              {
                  "visibility": "on"
              },
              {
                  "color": "#ffffff"
              },
              {
                  "lightness": 16
              }
          ]
      },
      {
          "featureType": "all",
          "elementType": "labels.icon",
          "stylers": [
              {
                  "visibility": "off"
              }
          ]
      },
      {
          "featureType": "administrative",
          "elementType": "geometry.fill",
          "stylers": [
              {
                  "color": "#fefefe"
              },
              {
                  "lightness": 20
              }
          ]
      },
      {
          "featureType": "administrative",
          "elementType": "geometry.stroke",
          "stylers": [
              {
                  "color": "#fefefe"
              },
              {
                  "lightness": 17
              },
              {
                  "weight": 1.2
              }
          ]
      },
      {
          "featureType": "landscape",
          "elementType": "geometry",
          "stylers": [
              {
                  "color": "#f5f5f5"
              },
              {
                  "lightness": 20
              }
          ]
      },
      {
          "featureType": "poi",
          "elementType": "geometry",
          "stylers": [
              {
                  "color": "#f5f5f5"
              },
              {
                  "lightness": 21
              }
          ]
      },
      {
          "featureType": "poi.park",
          "elementType": "geometry",
          "stylers": [
              {
                  "color": "#dedede"
              },
              {
                  "lightness": 21
              }
          ]
      },
      {
          "featureType": "road.highway",
          "elementType": "geometry.fill",
          "stylers": [
              {
                  "color": "#ffffff"
              },
              {
                  "lightness": 17
              }
          ]
      },
      {
          "featureType": "road.highway",
          "elementType": "geometry.stroke",
          "stylers": [
              {
                  "color": "#ffffff"
              },
              {
                  "lightness": 29
              },
              {
                  "weight": 0.2
              }
          ]
      },
      {
          "featureType": "road.arterial",
          "elementType": "geometry",
          "stylers": [
              {
                  "color": "#ffffff"
              },
              {
                  "lightness": 18
              }
          ]
      },
      {
          "featureType": "road.local",
          "elementType": "geometry",
          "stylers": [
              {
                  "color": "#ffffff"
              },
              {
                  "lightness": 16
              }
          ]
      },
      {
          "featureType": "transit",
          "elementType": "geometry",
          "stylers": [
              {
                  "color": "#f2f2f2"
              },
              {
                  "lightness": 19
              }
          ]
      },
      {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
              {
                  "color": "#e9e9e9"
              },
              {
                  "lightness": 17
              }
          ]
      },
      {
          "featureType": "water",
          "elementType": "geometry.fill",
          "stylers": [
              {
                  "color": "#72b1c8"
              },
              {
                  "gamma": "3.53"
              }
          ]
      }
  ];

  map.setOptions({styles: styles});


}
